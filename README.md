# Cookie clicker

Play cookie clicker on urbit.

Front-end is taken from https://github.com/ozh/cookieclicker, which is taken from the original game: http://orteil.dashnet.org/cookieclicker/.

The game itself is (so far) unmodified from the github source above.

At the moment, this is a very dumb app; it runs no agents and is just a way to create a launcher tile in Landscape for the game.  The game source code is unmodified from the [github source from above](https://github.com/ozh/cookieclicker).

However, I am going to add an agent that will store game save files.  Right now games are saved in the browser cookies, so if you ever clear your cookies your game will be lost.  Game saves are just a short string, so they can be easily stored by a Gall agent.


## Installation

```
|install ~dister-dozzod-wispem-wantex %cookie-clicker
```
